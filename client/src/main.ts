import 'bootstrap/js/dist/dropdown.js'
import 'bootstrap/js/dist/collapse.js'
import './sass/bootstrap.scss'
import { createApp } from 'vue'
import CountryFlag from 'vue-country-flag-next'
import { Table } from '@oruga-ui/oruga-next'
import App from './App.vue'
import router from './router'

const app = createApp(App)

app.component('CountryFlag', CountryFlag)

app.use(Table)

app.use(router)
app.mount('#app')

