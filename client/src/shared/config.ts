import { ServerConfig } from '../../../shared/models'
import { buildApiUrl } from './utils'

const basePath = '/api/v1/config'
let serverConfigPromise: Promise<ServerConfig>

function getConfig () {
  if (serverConfigPromise) return serverConfigPromise

  serverConfigPromise = buildConfigPromise()

  return serverConfigPromise
}

// ---------------------------------------------------------------------------

export {
  getConfig
}

// ---------------------------------------------------------------------------

function buildConfigPromise (): Promise<ServerConfig> {
  return fetch(buildApiUrl(basePath))
    .then(res => res.json())
}
