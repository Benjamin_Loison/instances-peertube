function buildApiUrl (path: string) {
  const normalizedPath = path.startsWith('/') ? path : '/' + path

  if (import.meta.env.PROD) return window.location.origin + normalizedPath

  return import.meta.env.VITE_APP_API_URL + normalizedPath
}

async function httpErrorToString (error: any) {
  const response = error.response as Response

  if (response) {
    if (response.status === 404) {
      return 'Not found.'
    }

    if (response.status === 500) {
      return 'Unknown server error.'
    }

    const json = await response.json()

    console.log(json)
    if (json.error) return json.error

    if (json.errors) {
      return Object.keys(json.errors)
        .map(k => json.errors[k].msg)
        .join('. ')
    }

    return response.text()
  }

  return 'Unknown error'
}

function formatBytes (value: number, decimals = 0) {
  if (value === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(value) / Math.log(k));

  return parseFloat((value / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function formatNumber (value: number) {
  const dictionary = [
    { max: 1000, type: '' },
    { max: 1000000, type: 'K' },
    { max: 1000000000, type: 'M' },
    { max: 1000000000000, type: 'G' },
    { max: 1000000000000000, type: 'T' },
  ]

  const format = dictionary.find(d => value < d.max) || dictionary[dictionary.length - 1]
  const calc = Math.floor(value / (format.max / 1000))

  return `${calc}${format.type}`
}

export {
  buildApiUrl,
  httpErrorToString,
  formatBytes,
  formatNumber
}

