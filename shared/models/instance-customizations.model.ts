export const enum InstanceCustomizations {
  NONE = 0,
  NAME = 1 << 0,
  SHORT_DESCRIPTION = 1 << 1,
  DESCRIPTION = 1 << 2,
  TERMS = 1 << 3,
  HOMEPAGE = 1 << 4
}
