import express from 'express'
import { query } from 'express-validator'
import { logger } from '../../helpers/logger.js'
import { areValidationErrors } from './utils.js'

const globalStatsValidator = [
  query('includeAll')
    .optional()
    .isBoolean()
    .toBoolean().withMessage('Should have a valid includeAll boolean'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.query }, 'Checking global stats parameters')

    if (areValidationErrors(req, res)) return
    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  globalStatsValidator
}
