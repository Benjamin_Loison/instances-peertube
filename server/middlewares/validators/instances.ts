import express from 'express'
import { body, param, query } from 'express-validator'
import { isHostValid } from '../../helpers/custom-validators/instances.js'
import { logger } from '../../helpers/logger.js'
import { InstanceModel } from '../../models/instance.js'
import { areValidationErrors } from './utils.js'

const instancesListValidator = [
  query('signup').optional().isBoolean(),
  query('healthy').optional().isBoolean(),
  query('nsfwPolicy').optional().isArray(),
  query('languages').optional().isArray(),
  query('categories').optional().isArray(),
  query('minUserQuota').optional().isInt().toInt(),

  query('customizations').optional().isInt().toInt(),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.query }, 'Checking instances list parameters')

    if (areValidationErrors(req, res)) return
    return next()
  }
]

const instanceHostsValidator = [
  query('since').optional().isISO8601().withMessage('Should have a valid since filter'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.query }, 'Checking instance hosts list parameters')

    if (areValidationErrors(req, res)) return
    return next()
  }
]

const instancesAddValidator = [
  body('host').custom(isHostValid).withMessage('Should have a valid host'),

  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.body }, 'Checking instances add parameters')

    if (areValidationErrors(req, res)) return

    req.body.host = (req.body.host || '').toLowerCase()

    const instance = await InstanceModel.loadByHost(req.body.host)

    if (instance) {
      return res
        .status(409)
        .json({
          error: `Instance ${req.body.host} was already added.`
        })
        .end()
    }

    return next()
  }
]

const instanceGetValidator = [
  param('host').custom(isHostValid).withMessage('Should have a valid host'),

  async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.params }, 'Checking instance get parameters')

    if (areValidationErrors(req, res)) return

    const instance = await InstanceModel.loadByHost(req.params.host)

    if (!instance) {
      return res
        .status(404)
        .json({
          error: `Instance ${req.params.host} not found.`
        })
        .end()
    }

    res.locals.instance = instance
    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  instancesAddValidator,
  instanceGetValidator,
  instanceHostsValidator,
  instancesListValidator
}
