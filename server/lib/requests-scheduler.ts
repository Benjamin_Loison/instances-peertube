import Bluebird from 'bluebird'
import { fetchInstance } from '../helpers/instance-requests.js'
import { logger } from '../helpers/logger.js'
import { CONCURRENCY_REQUESTS, SCHEDULER_INTERVAL } from '../initializers/constants.js'
import { HistoryModel } from '../models/history.js'
import { InstanceModel } from '../models/instance.js'

export class RequestsScheduler {

  private interval: NodeJS.Timer
  private isRunning = false

  private static instance: RequestsScheduler

  private constructor () { }

  enable () {
    logger.info('Enabling request scheduler.')
    this.interval = setInterval(() => this.execute(), SCHEDULER_INTERVAL)

    this.execute()
      .catch(err => logger.error({ err }, 'Error in request scheduler'))
  }

  disable () {
    clearInterval(this.interval)
  }

  async execute () {
    // Already running?
    if (this.isRunning === true) return

    this.isRunning = true

    logger.info('Running requests scheduler.')

    const badInstances: number[] = []
    const goodInstances: number[] = []

    const instances = await InstanceModel.listHostsWithId()

    await Bluebird.map(instances, async instance => {
      try {
        const { config, stats, about, connectivityStats } = await fetchInstance(instance.host)
        await InstanceModel.updateInstance({ id: instance.id, config, stats, about, connectivityStats })
        await HistoryModel.addEntryIfNeeded(instance.id, stats)

        goodInstances.push(instance.id)
        logger.info(`Updated ${instance.host} instance.`)
      } catch (err) {
        badInstances.push(instance.id)
        logger.warn({ err }, `Cannot update ${instance.host} instance.`)
      }
    }, { concurrency: CONCURRENCY_REQUESTS })

    await InstanceModel.updateInstancesScoreAndRemoveBadOnes(goodInstances, badInstances)

    this.isRunning = false
  }

  static get Instance () {
    return this.instance || (this.instance = new this())
  }
}
