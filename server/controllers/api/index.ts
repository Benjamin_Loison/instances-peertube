import express from 'express'
import { badRequest } from '../../helpers/utils.js'
import { instancesRouter } from './instances.js'
import { configRouter } from './config.js'

const apiRouter = express.Router()

apiRouter.use('/', configRouter)
apiRouter.use('/instances', instancesRouter)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }
