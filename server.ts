import { isTestInstance } from './server/helpers/core-utils.js'
if (isTestInstance()) {
  require('source-map-support').install()
}

import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import { join, resolve } from 'path'
import { URL } from 'url'
import { apiRouter } from './server/controllers/api/index.js'
import { feedsRouter } from './server/controllers/feeds.js'
import { logger } from './server/helpers/logger.js'
import { API_VERSION, CONFIG } from './server/initializers/constants.js'
// Initialize database and models
import { initDatabaseModels, sequelizeTypescript } from './server/initializers/database.js'
import { RequestsScheduler } from './server/lib/requests-scheduler.js'

const __dirname = new URL('.', import.meta.url).pathname;

const app = express()
app.set('trust proxy', 'loopback, uniquelocal')

initDatabaseModels(false)
  .then(() => onDatabaseInitDone())

app.use(morgan('combined', {
  stream: { write: logger.info.bind(logger) }
}))

app.use(bodyParser.json({
  type: [ 'application/json', 'application/*+json' ],
  limit: '500kb'
}))
app.use(bodyParser.urlencoded({ extended: false }))

// ----------- Views, routes and static files -----------

app.use(cors())

const apiRoute = '/api/' + API_VERSION
app.use(apiRoute, apiRouter)

app.use('/feeds/', feedsRouter)

// Static client files
app.use('/js/', express.static(join(__dirname, '../client/dist/js')))
app.use('/assets/', express.static(join(__dirname, '../client/dist/assets')))
app.use('/css/', express.static(join(__dirname, '../client/dist/css')))
app.use('/img/', express.static(join(__dirname, '../client/dist/img')))

app.use('/*', function (req, res) {
  return res.sendFile(join(__dirname, '../client/dist/index.html'))
})

// ----------- Errors -----------

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err['status'] = 404
  next(err)
})

app.use(function (err, req, res, next) {
  let error = 'Unknown error.'
  if (err) {
    error = err.stack || err.message || err
  }

  logger.error({ error }, 'Error in controller.')
  return res.status(err.status || 500).end()
})

// ----------- Run -----------

function onDatabaseInitDone () {
  const port = CONFIG.LISTEN.PORT

  sequelizeTypescript.sync()
    .then(() => {
      // ----------- Make the server listening -----------
      app.listen(port, () => {
        logger.info('Server listening on port %d', port)

        RequestsScheduler.Instance.enable()
      })
    })
}
