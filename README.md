# Application behind instances.joinpeertube.org

## Dev

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
```

Initialize the database:

```terminal
$ sudo -u postgres createuser -P peertube
$ sudo -u postgres createdb -O peertube peertube_instances
```

Then run simultaneously (for example with 3 terminals):

```terminal
$ tsc -w
```

```terminal
$ node dist/server
```

```terminal
$ cd client && npm run dev
```

Then open http://localhost:8080.

## Production

In the root of the cloned repo:

```terminal
$ git submodule update --init --recursive
$ yarn install --pure-lockfile
$ npm run build
$ sudo -u postgres createuser -P peertube
$ sudo -u postgres createdb -O peertube peertube_instances
$ node dist/server.js
```

## PeerTube auto follow & Global Search

If you don't want to run this application but would like to have your own index for the [PeerTube auto follow feature](https://docs.joinpeertube.org/#/admin-following-instances?id=automatically-follow-other-instances) or the [Global Search](https://framagit.org/framasoft/peertube/search-index), serve the following JSON format:

```json
{
  "total": 5,
  "data": [
    {
      "host": "video1.example.com"
    },
    {
      "host": "video2.example.com"
    },
    {
      "host": "video3.example.com"
    },
    {
      "host": "video4.example.com"
    },
    {
      "host": "video5.example.com"
    }
]
```
